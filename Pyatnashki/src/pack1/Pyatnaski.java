package pack1;

/**
 * 
 * <p>�����, ����������� ���� "��������".<p>
 * 
 * @author k.buhantsev
 * 
 */

public class Pyatnaski {

	/**
	*���� ������ <code>Pyatnaski</code>, �������� ���������� �����.
	 */
	private int MAX_ROWS;
	
	/**
	*���� ������ <code>Pyatnaski</code>, �������� ���������� ��������.
	*/
	private int MAX_COLLS;
	
	/**
	* ������� ������� <code>MAIN_MATRIX</code> ������ <code>Pyatnaski</code>.
	 */
	private Integer [][] MAIN_MATRIX;
	
	/**
	* ������ ���� <code>MAX_ROWS</code> ������ <code>Pyatnaski</code>  
	 * @return int - ����� �����.
	 */
	public int getMAX_ROWS() {
		return MAX_ROWS;
	}
	
	/**
	 * ������ ���� <code>MAX_COLLS</code> ������ <code>Pyatnaski</code>  
	 * @return int - ����� �������.
	 */
	public int getMAX_COLLS() {
		return MAX_COLLS;
	}

	/**
	* ����������� ������� <code>Pyatnaski</code> � �������������� ����������� ������� �������.
	 * 
	 * @param <code>maxRows</code> ����� ����� ������� �������
	 * @param <code>maxColls</code> ����� �������� ������� �������
	 */
	public Pyatnaski(int maxRows, int maxColls) {

		//�������������� ������������� ����� �������
		MAX_ROWS = maxRows;
		MAX_COLLS = maxColls;
		MAIN_MATRIX = new Integer [MAX_ROWS][MAX_COLLS];
		
		//����������
		
		//�������� ������� ����� �� 0 �� ���������� ��������� ������� �������.
		int [] arrOfValues = new int [MAX_COLLS * MAX_ROWS];
		for (int i = 0; i < arrOfValues.length; i++) {
			arrOfValues[i] = i;
		}
		
		//������������� ������� � �������� �������� ��
		int indOfElement, element;
		
		//�������� ���������� ���������:
		//�.�. ����� � �� ������ ������ � ��������� �������, ����� �������� �� 
		//� ����� ��� ������� �� �� �� ������� arrOfValues �� ���������� �������.
		//��������� ������� ����� ������ �� -1, ����� �� ������� ��� ������ ���.
		for (int i = 0; i < MAIN_MATRIX.length; i++) {
			for (int j = 0; j < MAIN_MATRIX[i].length; j++) {
					
				//��� ������ Math.random() ���������� ��������� ������. �� 0 �� ����� �������.
				indOfElement = (int) ((Math.random()*(arrOfValues.length)));
				//�������� ���� ������� � ������ �������� �� -1, �.�. �������� �� ��� ��� ���.
				element = arrOfValues[indOfElement];
				if (element < 0) {
					//���� ���� ������� ��� ��� ������, ������ ���� while �� ��� ���,
					//���� �� ������ � ������� ����� ��, ������� ��� �� ��������������.
					while (element < 0) {
						indOfElement = (int) ((Math.random() * (arrOfValues.length)));
						element = arrOfValues[indOfElement];
					}
				}
				//������������� �������� ������ -1.
				MAIN_MATRIX[i][j] = element;
				arrOfValues[indOfElement] = -1;
			
			}
		}
		
		//����� ���� ��� �� ������������, ���� 0 � ������ ��� �� NULL
		for (int i = 0; i < MAIN_MATRIX.length; i++) {
			for (int j = 0; j < MAIN_MATRIX[i].length; j++) {
				if (MAIN_MATRIX[i][j] == 0){
					MAIN_MATRIX[i][j] = null;
				} 
			}
		}	
	
	}

	
	/**
	 * ����� ��� ������ ���� NULL � �������� ����� ������ �����, ������, ������ ��� ����� ��������� 
	 * ������������� ������ ��.
	 * 
	 * @param row (int) - ������ ������ ���������� ��������. 
	 * @param col (int) - ������ ������� ���������� ��������.
	 * @return -1 ���� NULL �� ������, 0 ���� ������� �������� �������.
	 */
	public int rotate(int row, int col){
		
		//�������� �� ����������� ������ ���� �� ������, ��������� ������������� 
		if ((row + 1) <= (MAX_ROWS - 1)) {
		
			if (MAIN_MATRIX[row + 1][col] == null){
				//���� �� ������� ������� NULL, �������� ����� ���������������� ������ ���������
				//� ��.
				exchangePlace(row, col, row+1, col);
				return 0;
			}
					
		} 
		
		//�������� �� ����������� ������ ���� �� ������, ��������� ������������� 
		if ((row - 1) >= 0) {
			
			if (MAIN_MATRIX[row - 1][col] == null){
				exchangePlace(row, col, row-1, col);
				return 0;
			}
			
		}
		
		//�������� �� ����������� ������� ����� �� ������, ��������� ������������� 
		if ((col - 1) >= 0) {
			
			if (MAIN_MATRIX[row][col-1] == null){
				exchangePlace(row, col, row, col-1);
				return 0;
			}
			
		}
		
		//�������� �� ����������� ������� ������ �� ������, ��������� �������������
		if ((col + 1) < MAX_COLLS) {
			
			if (MAIN_MATRIX[row][col+1] == null){
				exchangePlace(row, col, row, col+1);
				return 0;
			}	
		}
	
		return -1;
		
	};
		
	/**
	* �����, ������� ��������������� ������ �������� �� �������.
	* � ������ ��� ��������� �������� ������� �������, �� ������ null �������.
	 * @param rowFrom 
	 * @param colFrom
	 * @param rowWhere
	 * @param colWhere
	 */
	private void exchangePlace(int rowFrom, int colFrom, int rowWhere, int colWhere){
		
		MAIN_MATRIX[rowWhere][colWhere] = MAIN_MATRIX[rowFrom][colFrom];
		MAIN_MATRIX[rowFrom][colFrom] = null;
		
	}
	
	//��� ������ �� �� �������.
	//���������������� ����� toString() ������ Pyatnaski ��� ������ �� �� �������. 
	@Override
	public String toString() {
		
		String result = "";
		for (int i = 0; i < MAIN_MATRIX.length; i++) {
			for (int j = 0; j < MAIN_MATRIX[i].length; j++) {
				result = result + MAIN_MATRIX[i][j] + " ";
			}
			result = result + "\n";
		}
		
		return result;
	}
		
}
