package bankSystem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;

import parser.ParserStaff;
import parser.ParserUniversal;

public class Staff extends Person{
	
	private String post;
	private String department;
	private Summ salary;
		
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((department == null) ? 0 : department.hashCode());
		result = prime * result + ((post == null) ? 0 : post.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Staff other = (Staff) obj;
		if (department == null) {
			if (other.department != null)
				return false;
		} else if (!department.equals(other.department))
			return false;
		if (post == null) {
			if (other.post != null)
				return false;
		} else if (!post.equals(other.post))
			return false;
		return true;
	}

	@Override
	public String toString() {
//		return "����� Staff:[�������=" + getSurename() + "; ���=" + getName() + "; ��������=" + getMiddleName() + 
//				" ���������=" + post + "; �����=" + department + "; ���.�����=" + salary + "]";
		return "����� Staff:[�������=" + getSurename() + "; ���=" + getName() + "; ��������=" + getMiddleName() + "]";
	}
	
	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public Summ getSalary() {
		return salary;
	}

	public void setSalary(Summ salary) {
		this.salary = salary;
	}

	public Staff(String surename, String name, String middleName,
			String post, String department, Summ salary) {
	
		super(surename, name, middleName);
		this.post = post;
		this.department = department;
		this.salary = salary;
		
	}
	
	public Staff() {
		
		super();
					
	}

	public static void main(String[] args) throws IOException{
		
		/*
		String [][] staffArray = {{"������", "�����", "������", "����������� ������", "����� - ����������", "0"},
								{"��������", "�����", "�����������", "������", "����� - �����", "0"},
								{"�����������", "������", "��������������", "��������������", "����� ��������", "0"},
								{"�������", "������", "��������", "���.������������", "�����-����������", "0"}};
		
		Staff [] staffs = Staff.makeMassStaff(staffArray);
		Mass.showArrayOfObjects(staffs);
		*/
		
//		Staff [] staffs = Staff.makeArrStaff("src/staff.in");
//		testArrStaff(staffs);
		
		
//		Staff staffs[] = null;
//		try {
//			staffs = ParserStaff.makeMassStaffs("src/staff.in");
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		for (int i = 0; i < staffs.length; i++) {
//			System.out.println(staffs[i]);
//		}
		
//		Staff [] staffs = Staff.makeArrStaff("src/staff.in");
//		testArrStaff(staffs);
		
//		testCollStaff(makeCollStaff("src/staff.in"));
//		testCollStaff(makeMapStaff("src/staff.in"));
		
		TreeMap<String, Staff> mapOfWorkers = makeTreeMapStaff("src/staff.in");
		
		HashMap<String, Staff> hashMapOfWorkers = new HashMap<String, Staff>();
		
		Set<Entry<String, Staff>> setOfStaffs = mapOfWorkers.entrySet();
		for (Entry<String, Staff> entry : setOfStaffs) {
			String [] stringKeys = entry.getKey().split("[-]");
			hashMapOfWorkers.put(stringKeys[1].trim() + "-" + stringKeys[2].trim()
					, entry.getValue());
		}
			
		System.out.println(hashMapOfWorkers);
		
		HashMap<String, Staff> resMap = Staff.getStaffsByProfessionBulk(hashMapOfWorkers, "�����");
			
		System.out.println(resMap);
		
	}
	
	public static ArrayList<Staff> getStaffsByProfession(HashMap<String, Staff> mapOfWorkers, String position) {
		
		ArrayList<Staff> arrayOfStaffs = new ArrayList<Staff>();
		
		Set<Entry<String, Staff>> setOfStaffs = mapOfWorkers.entrySet();
		for (Entry<String, Staff> entry : setOfStaffs) {
			if (entry.getKey().contains(position)) {
				arrayOfStaffs.add(entry.setValue(entry.getValue()));
			}
		}		
		
		return arrayOfStaffs;
		
	}
	
    public static ArrayList<Staff> getStaffsByProfessionIterator(HashMap<String, Staff> mapOfWorkers, String position) {
		
		ArrayList<Staff> arrayOfStaffs = new ArrayList<Staff>();
		
		for(Iterator iterator = mapOfWorkers.entrySet().iterator() ; iterator.hasNext(); ){
			Entry<String, Staff> entry = (Entry<String, Staff>)iterator.next();
			if (entry.getKey().contains(position)) {
				arrayOfStaffs.add(entry.setValue(entry.getValue()));
			}
		} 
		
		TreeMap<String, Staff> treeMapOfWorkers = new TreeMap<String, Staff>(mapOfWorkers);
		
		
		
		return arrayOfStaffs;
		
		/*
		TreeMap<String, Staff> treeMapOfWorkers = new TreeMap<String, Staff>(mapOfWorkers);
		TreeMap<String, Staff> newMap = new TreeMap<String, Staff>();
		
		Set<Entry<String, Staff>> setOfStaffs = treeMapOfWorkers.entrySet();
		for (Entry<String, Staff> entry : setOfStaffs) {
			String firstKey = entry.getKey();
			String[] arrOfKey = firstKey.split("[-]");
			String newKey = arrOfKey[arrOfKey.length-2].trim() + "-" + arrOfKey[arrOfKey.length-1].trim();
			newMap.put(newKey, entry.getValue());
		}
		
		NavigableMap<String, Staff> resMap = newMap.subMap(newMap.ceilingEntry(position + "-0").getKey(), true,
				newMap.floorEntry(position + "-9999999").getKey(), true);
		
		System.out.println(resMap);
		
		
		return arrayOfStaffs;
		
		*/
		
	}
    
    /**
     * 
     * @param mapOfWorkers
     * @param position
     * @return
     */
    public static HashMap<String, Staff> getStaffsByProfessionBulk(HashMap<String, Staff> mapOfWorkers, String position) {
						
		TreeMap<String, Staff> treeMapOfWorkers = new TreeMap<String, Staff>(mapOfWorkers);
			
		NavigableMap<String, Staff> navMap = treeMapOfWorkers.subMap(position + "-0", true, position + "-99999", true);
		
		return new HashMap<String, Staff>(navMap);
			
	}
			
	/**
	 * �����, ��� �������� ������� ���� <b>Staff[]</b>
	 * @param arrOfWorkers ���� <b>String[][]</b>, ��������� ������.
	 * @return arrOfStaffs ���� <b>Staff[]</b>, ���������� ������ �������� ���� <b>Staff</b>.
	 */
	public static Staff[] makeMassStaff(String[][] arrOfWorkers) {
		
		Staff[] arrOfStaffs = new Staff[arrOfWorkers.length];
		
		for (int i = 0; i < arrOfWorkers.length; i++) {
			Staff staff = new Staff(arrOfWorkers[i][0], arrOfWorkers[i][1], arrOfWorkers[i][2], 
					"", arrOfWorkers[i][4], new Summ(Long.parseLong(arrOfWorkers[i][5]), "UAH"));
			arrOfStaffs[i] = staff;
		}
		
		return arrOfStaffs;
		
	}
	
	public static Staff[] makeArrStaff(String fileName) throws IOException{
		
		ParserUniversal.parserManagerClient(fileName);
		
		return ParserUniversal.arrStaff;
		
	}
	
	public static ArrayList<Staff> makeCollStaff(String fileName) throws IOException{
	
		List<Staff> listStaff = Arrays.asList(Staff.makeArrStaff("src/staff.in"));
		
		return new ArrayList<Staff>(listStaff);
		
	}
	
	public static TreeMap<String, Staff> makeTreeMapStaff(String fileName) throws IOException{
		
		List<Staff> listStaff = Arrays.asList(Staff.makeArrStaff("src/staff.in"));
		
		return makeSetFromList(listStaff);
		
	}
	
	private static TreeMap<String, Staff> makeSetFromList(List<Staff> listStaff) {
		
		TreeMap<String, Staff> mapOfWorkers = new TreeMap<String, Staff>();
		int i = 0;
		for (Staff staff : listStaff) {
			mapOfWorkers.put(staff.department + "-" + staff.post + "-" + ++i,
					staff);
		}
		
		return mapOfWorkers;
		
	}
	
	private static void testArrStaff(Staff[] staffs) {
		
		for (int i = 0; i < staffs.length; i++) {
			System.out.println(staffs[i]);
		}
		
	}
	
	public static void testCollStaff(ArrayList<Staff> staffs) {
	
		for (Staff staff : staffs) {
			System.out.println(staff);
		}
		
	}
	
	public static void testCollStaff(Map<String, Staff> staffs) {
		
		/*
		Set<Entry<String, Staff>> entryStaffs= staffs.entrySet();
		for (Entry<String, Staff> entry : entryStaffs) {
			System.out.println(entry);
		}
		*/
		
		System.out.println(staffs);
		
	}
		
}
