package calculator;

import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.swing.JButton;
import javax.swing.JOptionPane;

public class CalculatorProcess implements ActionListener, KeyListener, MouseListener{

	private void keyboardProcess(String eventString) {
		
		String[] arrOfNumbers = {"1","2","3","4","5","6","7","8","9","0"};
		String[] arrOfOperands = {"+","-","*","/"};
		
		if (stringInArray(arrOfNumbers, eventString)) {
			
			System.out.println("���� ������ "+eventString);
			
			processClickNumberButton(eventString);			
			renewMainTextfield();
			
		} else if (stringInArray(arrOfOperands, eventString)){
			
			System.out.println("���� ������ "+eventString);
			
			processClickOfOperand(eventString);
			renewMainTextfield();
			
		} else if (eventString.equals("=")){
			
			System.out.println("���� ������ "+eventString);
			
			calculate();
			
		} else if (eventString.equalsIgnoreCase("C")){
			
			System.out.println("���� ������ "+eventString);
			resetMainTextfield();
			
		} else if (eventString.equals("CE")) {
			
			System.out.println("���� ������ "+eventString);
			Calculator.operand2 = "";
			renewMainTextfield();
		
		} else if (eventString.equals(",") || eventString.equals(".")){
			
			System.out.println("���� ������ "+eventString);
			processClickButtonComa();
			
		} else if (eventString.equals("Enter")) {
			
			System.out.println("���� ������ "+eventString);
			calculate();	
			
		} else if (eventString.equals("Delete") || eventString.equals("MC")) {
			
			//�������� ������ ������������
			System.out.println("���� ������ "+eventString);
			Calculator.memory = 0D;
							
		} else if (eventString.equals("MR") || eventString.equals("Insert")) {
			
			//���������� ����� �� ������
			System.out.println("���� ������ "+eventString);
			resetMainTextfield();
			setResultToTextField(Calculator.memory);
			
			
		} else if (eventString.equals("M+") || eventString.equals("Page Up")) {
			
			//��������� ����� � ����� � ������
			System.out.println("���� ������ "+eventString);
			
			calculate();
			Double numberFromTextField = Double.parseDouble(Calculator.textField.getText());
			Calculator.memory = Calculator.memory + numberFromTextField;
			
			
		} else if (eventString.equals("M-") || eventString.equals("Page Down")) {
			
			//������� ����� �� ������ 
			System.out.println("���� ������ "+eventString);
			
			calculate();
			Double numberFromTextField = Double.parseDouble(Calculator.textField.getText());
			Calculator.memory = Calculator.memory - numberFromTextField;
			
			
		} else if (eventString.equals("MS") || eventString.equals("Pause")) {
			
			//��������� ����� � ������
			System.out.println("���� ������ "+eventString);
			
			calculate();
			if ( ! Calculator.textField.getText().equals("") ) {
				Double numberFromTextField = Double.parseDouble(Calculator.textField.getText());
				Calculator.memory = numberFromTextField;
			}
					
		} else if (eventString.equals("Backspace")) {
			
			processClickBackspace(eventString);
			
		}
		
	}
	
	private void keyboardProcess(KeyEvent event) {
		
		if ((event.getModifiers() & event.CTRL_MASK) != 0) {

			if (event.getKeyCode() == event.VK_PLUS) {
				
				calculate();
				Double numberFromTextField = Double.parseDouble(Calculator.textField.getText());
				Calculator.memory = Calculator.memory + numberFromTextField;
				
				System.out.println("���� ������ CTRL + '+'");				
				
			} else if (event.getKeyCode() == event.VK_C) {
				
				resetMainTextfield();
				
				System.out.println("���� ������ CTRL + C");
				
			} else if (event.getKeyCode() == event.VK_MINUS) {
				
				calculate();
				Double numberFromTextField = Double.parseDouble(Calculator.textField.getText());
				Calculator.memory = Calculator.memory - numberFromTextField;
				
				System.out.println("���� ������ CTRL + '-'");
				
			} 
			
		}
		
	}
	
	//ActionListener
	@Override
	public void actionPerformed(ActionEvent event) {
		
		String actionCommand = event.getActionCommand();
		keyboardProcess(actionCommand);
		
	}//ActionListener
	
	//KeyListener
	@Override //������� ����� �������, �������� � ����������
	public void keyPressed(KeyEvent event) {
	
		String keyEvent = String.valueOf(event.getKeyChar());
		if (allowLettersAndSymbols(keyEvent)) {
			keyboardProcess(keyEvent);
		} else {
			keyEvent = event.getKeyText(event.getExtendedKeyCode());
			if ( (event.getModifiers() & event.CTRL_MASK) != 0 ) {
				keyboardProcess(event);
			} else if (allowExtendedLettersAndSymbols(keyEvent)) {
				keyboardProcess(keyEvent);
			} else {
				JOptionPane.showMessageDialog(event.getComponent(),
						"������ ������������ ������ "+keyEvent, "������ �����", JOptionPane.ERROR_MESSAGE);
			}
		}
		
	}//KeyListener

	//MouseListener
	@Override
	public void mouseClicked(MouseEvent event) {

		String actionCommand = event.getMouseModifiersText(event.getModifiers());
		if (actionCommand.equalsIgnoreCase("Ctrl+Button1")) {
		
			JButton clickedButton = (JButton)event.getComponent();
			keyboardProcess(clickedButton.getActionCommand());
			calculate();
			
		} else if (actionCommand.equalsIgnoreCase("Button1")) {
			
			JButton clickedButton = (JButton)event.getComponent();
			keyboardProcess(clickedButton.getActionCommand());
			
		}
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}
	
	@Override //���������� ����� �������, �������� � ����������
	public void keyReleased(KeyEvent event) {
		
		
	}//KeyListener

	@Override //������� ������ �������� �������
	public void keyTyped(KeyEvent event) {
		
		
	}//KeyListener
	
	private boolean allowLettersAndSymbols(String eventString) {
		
		String[] arrOfAllSymbols = {"1","2","3","4","5","6","7","8","9","0",
				"+","-","*","/", "=", "C", "CE", ",", "."};
		
		if (stringInArray(arrOfAllSymbols, eventString)) {
			return true;
		} else {
			return false;
		}
		
	}
	
	private boolean allowExtendedLettersAndSymbols(String eventString) {
		
		String[] arrOfAllSymbols = {"Enter", "Delete", "Page UP", "Insert", "Pause",
				"Page Down", "Backspace", "Ctrl", "c"};
		
		if (stringInArray(arrOfAllSymbols, eventString)) {
			return true;
		} else {
			return false;
		}
		
	}
	
	private void processClickBackspace(String eventString) {
		
		if (Calculator.operation == "") {
			if ( (Calculator.operand1 != "") ) {
				Calculator.operand1 = Calculator.operand1.substring(0, Calculator.operand1.length()-1);
			}
		} else {
			if ( (Calculator.operand2 != "") ) {
				Calculator.operand2 = Calculator.operand2.substring(0, Calculator.operand2.length()-1);
			}
		}
		
		renewMainTextfield();
						
	}
	
    private void processClickButtonComa() {
		
		if (Calculator.operation == "") {
			if ( (Calculator.operand1 != "") && (Calculator.operand1.indexOf(".") == -1) ) {
				Calculator.operand1 = Calculator.operand1 + ".";
			}
		} else {
			if ( (Calculator.operand2 != "") && (Calculator.operand2.indexOf(".") == -1) ) {
				Calculator.operand2 = Calculator.operand2 + ".";
			}
		}
		
		renewMainTextfield();
				
	}

	private void processClickOfOperand(String eventString) {

		if (Calculator.operation == "") {
			if (Calculator.operand1 != "") {
				Calculator.operation = eventString;
			}
		} else if (Calculator.operation != "") {
			if (Calculator.operand2 == "") {
				Calculator.operation = eventString;
			} else {
				calculate();
				Calculator.operation = eventString;
			}
		} 
		
		renewMainTextfield();
		
	}

	private void processClickNumberButton(String buttonString) {

		if (Calculator.operation == "") {
			Calculator.operand1 = Calculator.operand1 + buttonString;
		} else if (Calculator.operation != ""){
			Calculator.operand2 = Calculator.operand2 + buttonString;
		} 
		renewMainTextfield();
		
	}

	private boolean stringInArray(String[] inputArray, String findingString) {
		
		for (int i = 0; i < inputArray.length; i++) {
			if (inputArray[i].equalsIgnoreCase(findingString)) {
				return true;
			}
		}
		
		return false;
		
	}
	
	private void renewMainTextfield() {
		
		Calculator.textField.setText(Calculator.operand1+Calculator.operation+Calculator.operand2);
		
	}

	private void resetMainTextfield() {
		
		Calculator.operand1 = "";
		Calculator.operand2 = "";
		Calculator.operation = "";
		Calculator.textField.setText("");
		
	}
	
	private void setResultToTextField(Double result) {
		
		if (Double.compare(result, Math.round(result)) == 0 ) { //�����
			String resultString = String.valueOf(result);
			Calculator.textField.setText(resultString.substring(0, resultString.indexOf(".")));
		} else { //�� �����
			Calculator.textField.setText(String.format("%.2f", result));
		}
		
		Calculator.operand1 = String.valueOf(result);
		
	}
	
	private void calculate() {
		
		StringBuffer javascript = null;
		ScriptEngine runtime = null;

		try {
			
			runtime = new ScriptEngineManager().getEngineByName("javascript");
			javascript = new StringBuffer();
			javascript.append(Calculator.operand1+Calculator.operation+Calculator.operand2);
			
			double result = (Double) runtime.eval(javascript.toString());
			setResultToTextField(result);
			clearCerviceFields();
			Calculator.operand1 = String.valueOf(result);
			
		} catch (Exception e) {
			
			System.out.println(e.getMessage());
			resetMainTextfield();
			
		}
		
	}
	
	private void clearCerviceFields() {

		Calculator.operand1 = "";
		Calculator.operand2 = "";
		Calculator.operation = "";
		
	}


		
}
