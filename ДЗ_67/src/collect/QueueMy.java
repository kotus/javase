package collect;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.NavigableSet;
import java.util.SortedSet;
import java.util.TreeSet;

public class QueueMy {

	public static void main(String[] args) {

		Deque<Integer> deque = new LinkedList<Integer>();
		
		for (int i = 1; i <= 10; i++) {
			deque.add(i);
		}
		
		deque.addFirst(5);
		deque.addLast(5);
		
		System.out.println(deque);
		
		//�������� �� ������ ������� �������� � ����� ����������: ���� ����� ������ 5-��, �� ������� ���
		//� ������ � ������ less5, ����� ������� ������� � ���������� � ������ bigger5
		
		TreeSet<Integer> less5 = new TreeSet<Integer>();
		LinkedList<Integer> bigger5 = new LinkedList<Integer>();
		
		for (Iterator iterator = deque.iterator(); iterator.hasNext();) {
			Integer currentElement = (Integer) iterator.next();
			if (currentElement < 5) {
				less5.add(currentElement);
			} else if (currentElement > 5){
				bigger5.add(currentElement);
			}
		}
		
		System.out.println("less5: " + less5);
		System.out.println("bigger5: " + bigger5);
		
		System.out.println("***** ����� ���������� *****");
		//������������� ��� ��������� � �������� ������� � ������� ������ ���������
		Collections.sort(bigger5, new SORT_IN_REVERSE_ORDER());
		System.out.println(bigger5);		
		
		ArrayList<Integer> arrayList = new ArrayList<Integer>(less5);
		Collections.sort(arrayList, new SORT_IN_REVERSE_ORDER());
		System.out.println(arrayList);
				
		// �������� � ������ 3 ����� � �����. � �������� �� �� �������� �����: 
		//LIFO � ��� ��������� ����� ��� ������ �����.
		System.out.println("***** �������� 3 ��. � ����� *****");
		less5.add(20);less5.add(21);less5.add(22);
		System.out.println(less5);
		
		System.out.println("***** LIFO *****");
		ArrayList<Integer> less5_2 = new ArrayList<Integer>();
		for (int i = 0; i < 3; i++) {
			less5_2.add(less5.pollLast());
		}
		
		System.out.println("less5: " + less5);
		System.out.println("less5_2: " + less5_2);
		
	}

}

class SORT_IN_REVERSE_ORDER implements Comparator<Integer>{

	@Override
	public int compare(Integer o1, Integer o2) {
		return -(o1.compareTo(o2));
	}
	
}
