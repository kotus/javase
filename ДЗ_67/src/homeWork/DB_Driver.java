package homeWork;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.postgresql.Driver;

public class DB_Driver {

	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {

		Class jdbcDriver = Class.forName("org.postgresql.Driver");
		
		Connection con = DriverManager.getConnection("jdbc:postgresql://localhost", "postgres", "postgres");
		
		String dbName = "b2";
		
		PreparedStatement preparedStatement = con.prepareStatement("select * from postgres");

	}

}
