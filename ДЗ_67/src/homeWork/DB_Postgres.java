package homeWork;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.TreeSet;

public class DB_Postgres {

	public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException{

		DB_Postgres db_Postgres = new DB_Postgres();
		
//		db_Postgres.readTable("b2", "bankomats");
//		db_Postgres.readTable("b2", "bankomats");
		
//		db_Postgres.readTableSQL("b2", "SELECT \"numInvent\", \"valutaCode\", summ FROM bankomats;");
		
//		db_Postgres.createFileBankomatsIn();
		
//		db_Postgres.createFileBankCardsIn();
		
//		db_Postgres.create10numbers("b_card");
		
//		db_Postgres.change10numbers("b_card");
		
//		db_Postgres.delete6and9numbers("b_card");
		
//		db_Postgres.getAllAndShow("b_card");
		
//		db_Postgres.generateFileWithPeople();
		
		db_Postgres.createFileBankCardsIn();

	}
	
	private void readTable(String DBname, String TableName) throws ClassNotFoundException, SQLException{
		
		Class.forName("org.postgresql.Driver");
		Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/" + DBname, "postgres", "postgres");

		String strSQL = "SELECT * FROM " + TableName;
		
		PreparedStatement ps = conn.prepareStatement(strSQL);
		try {
			ResultSet res = ps.executeQuery();
			int numColl = res.getMetaData().getColumnCount();
			while (res.next()) {
				String resString = "";
				for (int i = 1; i <= numColl; i++) {
					resString = resString + (res.getString(i) + "\t | ");
				}
				System.out.println(resString);
				writeLogToDisk(resString, "src/readTable.log");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (ps != null) {
				ps.close();
			} 
		}
			
	}
	
	private void readTableSQL(String DBname, String strSQL) throws ClassNotFoundException, SQLException{
		
		Class.forName("org.postgresql.Driver");
		Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/" + DBname, "postgres", "postgres");

		PreparedStatement ps = conn.prepareStatement(strSQL);
		try {
			ResultSet res = ps.executeQuery();
			int numColl = res.getMetaData().getColumnCount();
			while (res.next()) {
				String resString = "";
				for (int i = 1; i <= numColl; i++) {
					resString = resString + (res.getString(i) + "\t | ");
				}
				System.out.println(resString);
				writeLogToDisk(resString, "src/readTable.log");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (ps != null) {
				ps.close();
			} 
		}
			
	}
	
	private boolean writeLogToDisk(String stringToWrite, String filePath){
		
		SimpleDateFormat dateFormatYear = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat dateFormatTime = new SimpleDateFormat("HH-mm-ss-S");
		
		String resultString = dateFormatYear.format(new Date(System.currentTimeMillis()))+ " " +
				dateFormatTime.format(new Date(System.currentTimeMillis()))+ "\t | " + stringToWrite;
				
		FileWriter fileWriter = null;
		try {
			
			fileWriter = new FileWriter(new File(filePath), true);
			fileWriter.write(resultString + "\n");
			fileWriter.close();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			return false;
			
		}
		
		return true;
			
	}
	
	private void createFileBankomatsIn() throws IOException {
		
		BufferedReader bufferedReader = new BufferedReader(new FileReader("src/�����-������������.txt"));
		FileWriter fileWriter = new FileWriter(new File("src/Bankomats.in"), true);
		
		String strFromFile = bufferedReader.readLine();
		
		StringTokenizer tokenizer = new StringTokenizer(strFromFile, "�\t");

		int i = 0;
		Random random = new Random();
		
		while (tokenizer.hasMoreElements()) {
			
			String resultString = "";
			String stringAddress = tokenizer.nextToken() + "�";
			
			resultString = ++i + " | " + "1" + " | " + stringAddress.trim() + " | " + (random.nextInt(3) + 1) + 
					" | " + random.nextInt(100000) + " | " + "yes";
			
			try {
				fileWriter.write(resultString + "\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
		fileWriter.close();
		bufferedReader.close();
		
	}

	
	private void createFileBankCardsIn() throws IOException {
		
		FileWriter fileWriter = new FileWriter(new File("src/master-visa.txt"), true);
		
		Random random = new Random();
		
		long max = 9999999999999999L;
		long min = 1000000000000000L;
		
		GregorianCalendar calendar = (GregorianCalendar) GregorianCalendar.getInstance();
		calendar.add(GregorianCalendar.YEAR, 2);
		
		SimpleDateFormat dateFormatYear = new SimpleDateFormat("yyyy/MM/dd");
		String dateExp = dateFormatYear.format(calendar.getTime());
				
		for (int i = 1; i <= 50; i++) {
			
			String resultString = "";
						
//			resultString = i + " | " + String.format("%.0f", (min + (Math.random() * (max-min))) ) + " | " +
//					(random.nextInt(899) + 100) + " | " + dateExp + " | " + (random.nextInt(899) + 100) + " | " + 
//					((random.nextInt(3) + 1))  + " | " + random.nextInt(100000);
			
			resultString = String.format("%.0f", (min + (Math.random() * (max-min))) ) + "|" +
					dateExp + "|" + 
					(random.nextInt(899) + 100) + "|" + 
					((random.nextInt(3) + 1))  + "|" + 
					random.nextInt(100000);
			
			System.out.println(resultString);
			
			
			try {
				fileWriter.write(resultString + "\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
		fileWriter.close();
				
	}
	
	//3.1. ������� �����, ������� � ���� �� ������� ������ ������� ������� 10 �������, ������, �� 0 �� 20.
	private void create10numbers(String DBname) throws SQLException, ClassNotFoundException {
		
		Class.forName("org.postgresql.Driver");
		Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/" + DBname, "postgres", "postgres");

		Statement statement = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
		ResultSet res = statement.executeQuery("SELECT * FROM \"�����_������\";");
		try {
			for (int i = 0; i <= 20; ) {
				res.moveToInsertRow();
				res.updateInt(1, i);
				res.insertRow();
				i = i + 2;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (statement != null) {
				statement.close();
			} 
		}
		
	}
	
	//�������� �����, ������� ������� ������ � ������ ������ ���, ����� ��� ��� ������, �� 1 �� 10.
	private void change10numbers(String DBname) throws SQLException, ClassNotFoundException {
		
		Class.forName("org.postgresql.Driver");
		Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/" + DBname, "postgres", "postgres");

		Statement statement = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
		ResultSet res = statement.executeQuery("SELECT * FROM \"�����_������\";");
		try {
			for (int i = 1; i <= 10; i++) {
				res.next();
				res.updateInt(1, i);
				res.updateRow();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (statement != null) {
				statement.close();
			} 
		}
		
	}
	
	private void delete6and9numbers(String DBname) throws SQLException, ClassNotFoundException {
		
		Class.forName("org.postgresql.Driver");
		Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/" + DBname, "postgres", "postgres");

		Statement statement = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
		ResultSet res = statement.executeQuery("SELECT ����� FROM �����_������ WHERE ����� = 6 OR ����� = 9;");
		try {
			while (res.next()) {
				res.deleteRow();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (statement != null) {
				statement.close();
			} 
		}
		
	}
	
	private void getAllAndShow(String DBname) throws SQLException, ClassNotFoundException {
		
		Class.forName("org.postgresql.Driver");
		Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/" + DBname, "postgres", "postgres");

		PreparedStatement ps = conn.prepareStatement("SELECT * FROM �����_������;");
		try {
			ResultSet res = ps.executeQuery();
			while (res.next()) {
				System.out.println("� ���:" + res.getRow() +" - "+ res.getInt(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (ps != null) {
				ps.close();
			} 
		}
		
	}
	
	private void generateFileWithPeople() throws IOException {
		
		BufferedReader strBuf = new BufferedReader(new FileReader("src/names.txt"));
		String str;
		ArrayList<String> listNames = new ArrayList<String>();
		
		while ((str = strBuf.readLine()) != null) {
			if(str.startsWith("-")) continue;
			listNames.add(str);
		}
		
		ArrayList<String> listShureNames = new ArrayList<String>();
		String [] sufficsFIO = {"��", "��", "����", "��", "��"};
		for (String stringName : listNames) {
			for (int i = 0; i < sufficsFIO.length; i++) {
				listShureNames.add(stringName + sufficsFIO[i]);
			}
		}
		
		ArrayList<String> middleNames = new ArrayList<String>();
		strBuf = new BufferedReader(new FileReader("src/names.txt"));
		boolean underline = true;
		while ((str = strBuf.readLine()) != null) {
			if(str.startsWith("-")) {
				underline = false;
				continue;
			};
			if (underline) {
				middleNames.add(str + "����");
			} else {
				middleNames.add(str + "����");
			}
		}
		
		ArrayList<String> listFIO = new ArrayList<String>();
		
		for (String shureName : listShureNames) {
			for (String name : listNames) {
				for (String middleName : middleNames) {
					listFIO.add(shureName + " " + name + " " + middleName);
				}
			}
		}
		
		String [] rusAlphabet = {"�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�",
				"�","�","�","�","�","�","�","�","�","�","�"}; //28
		
		Random random = new Random();
			
		TreeSet<String> treeSetPass = new TreeSet<String>();
		while ( treeSetPass.size() != listFIO.size() ) {
			String numPass = "" + rusAlphabet[random.nextInt(27) + 1] + rusAlphabet[random.nextInt(27) + 1] + 
					String.format("%.0f", (100000 + (Math.random() * (999999-100000))) );
			treeSetPass.add(numPass);
		}
		
		ArrayList<String> listOfNumPass = new ArrayList<String>(treeSetPass);
		
		ArrayList<Long> arrayListIdCodes = new ArrayList<Long>();
		for (int i = 1; i <= listFIO.size(); i++) {
			
			Long newIdCode = (long) (1000000000 + (Math.random() * (9999999999L-1000000000)));
			if (arrayListIdCodes.contains(newIdCode)) {
				while (arrayListIdCodes.contains(newIdCode)) {
					newIdCode = newIdCode + 1;
				}
			}
			arrayListIdCodes.add(newIdCode);
			
		}

		//���:������� ����� �����������	�/�:2700700050618	�������:��34001	zip:65022	���:126540321
		//13720
		
		FileWriter fileWriter = new FileWriter(new File("src/ClientsAll.txt"), true);
		
		for (int i = 0; i < listFIO.size(); i++) {
			
//			String resString = "���:" + listFIO.get(i) + "\t" + "�������:" + listOfNumPass.get(i) + "\t" +
//					String.format("%.0f", (10000 + (Math.random() * (99999-10000) ) )) + "\t" +
//					arrayListIdCodes.get(i);
			String resString = (i+1) + "|" + listFIO.get(i) + "|" + listOfNumPass.get(i) + "|" +
			String.format("%.0f", (10000 + (Math.random() * (99999-10000) ) )) + "|" +
			arrayListIdCodes.get(i);
			fileWriter.write(resString + "\n");
			fileWriter.flush();
		}
		
		System.out.println("done.");
			
		//13609
		
	}
	
}
