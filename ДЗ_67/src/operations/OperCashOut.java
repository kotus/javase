package operations;

import bankSystem.BankCard;
import bankSystem.Bankomat;

public class OperCashOut extends Operation{

	private Bankomat side1;
	private BankCard side2;
	
	public OperCashOut(Long ID, String date, String time, String text, 
			Bankomat side1, BankCard side2) {
		
		super(ID, date, time, text, side1, side2);
		super.setOperSub(this);
		this.side1 = side1;
		this.side2 = side2;
		
	}

}
