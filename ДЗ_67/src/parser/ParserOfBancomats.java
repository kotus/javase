package parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import bankSystem.Bankomat;

public class ParserOfBancomats {
	
	public static Bankomat[] createArrayOfBancomats(String path) throws IOException{
		
		BufferedReader strBuf = new BufferedReader(new FileReader(path));
		strBuf.mark((int) new File(path).length() + 1);
		String str;
		int count = 0;
		while ((str = strBuf.readLine())!=null) {
			if (str.trim().equals("")) continue;
			count++;
		}
		
		Bankomat[] arrOfBankomats = new Bankomat[count];
		
		strBuf.reset();// = new BufferedReader(new FileReader(path));
		
		int i = 0;
		while ((str = strBuf.readLine()) != null) {
			
			String numInvent;
			String address;
			Long summ;
			String valuta;
			
			StringTokenizer tokenizer = new StringTokenizer(str, ",");
			while (tokenizer.hasMoreElements()) {
				
				numInvent =  tokenizer.nextToken().trim();
				
				address = tokenizer.nextToken("�,").trim();
				address = address.substring(1, address.length()).
						replace(address.substring(address.indexOf(" "), address.lastIndexOf(" ") + 1), " ");
								
				tokenizer.nextToken(",\t");
				
				summ = Long.parseLong(tokenizer.nextToken(",\t").trim());
				
				valuta = tokenizer.nextToken().trim();
								
				arrOfBankomats[i] = new Bankomat(numInvent, address, summ, valuta);
				
				i++;
				
			}
					
		}
		
		return arrOfBankomats;
		
	}
	
    public static ArrayList<Bankomat> createCollOfBancomats(String path) throws IOException{
		
		BufferedReader strBuf = new BufferedReader(new FileReader(path));
				
		ArrayList<Bankomat> listOfBankomats = new ArrayList<Bankomat>();
		
		String str;
		
		int i = 0;
		while ((str = strBuf.readLine()) != null) {
			
			String numInvent;
			String address;
			Long summ;
			String valuta;
			
			StringTokenizer tokenizer = new StringTokenizer(str, ",");
			while (tokenizer.hasMoreElements()) {
				
				numInvent =  tokenizer.nextToken().trim();
				
				address = tokenizer.nextToken("�,").trim();
				address = address.substring(1, address.length()).
						replace(address.substring(address.indexOf(" "), address.lastIndexOf(" ") + 1), " ");
								
				tokenizer.nextToken(",\t");
				
				summ = Long.parseLong(tokenizer.nextToken(",\t").trim());
				
				valuta = tokenizer.nextToken().trim();
								
				listOfBankomats.add(new Bankomat(numInvent, address, summ, valuta));
				
				i++;
				
			}
					
		}
		
		return listOfBankomats;
		
	}
	
	
}
